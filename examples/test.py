from dblogging import Logger, HtmlLogGenerator
from examples.custom_logger import LogTags
import threading
import time
import os
from datetime import datetime, timedelta
import random


logger = Logger()
logger.log_tags = LogTags
logger.log_path = f'{os.path.dirname(__file__)}/logs/log_{time.strftime("%Y%m%d%H%M%S")}'


@logger.wrap_class(
    log_tag=LogTags.test_1,
    func_regex_exclude='_.*'
)
class LoggerExample:
    _class_var = 0

    def __init__(self, x: int):
        self._value = random.randrange(10) + x

    @property
    def class_var(self):
        logger.log(f'Getter. {self._class_var=}')
        return self._class_var

    @class_var.setter
    def class_var(self, value: int):
        self._class_var = value
        logger.log(f'Setter: {self._class_var=}')

    @logger.wrap_func(
        log_tag=LogTags.test_2,
        mask_input_regexes=['.*'],
        mask_output=True
    )
    def override_log_wrapper(self, secret_number: int):
        logger.log(
            msg=f'The logger wrapper for this method is overridden. '
                f'Inputs and outputs of this method should be masked.',
            log_tag=LogTags.test_2
        )
        return {'Secret': secret_number + self._value}

    def recursive_function(self, number: int):
        logger.log(
            msg=f'The logger should wrap this method with wrap_class().',
            log_tag=LogTags.test_1
        )
        if number <= 0:
            return 0
        else:
            self._pause()
            return number + self.recursive_function(number - 1)

    @classmethod
    def class_method(cls, value=2):
        logger.log(
            msg=f'The logger should wrap this method with wrap_class().',
            log_tag=LogTags.test_1
        )
        cls._pause()
        return {
            'value': value,
            'cls': cls(value)
        }

    @staticmethod
    @logger.wrap_func(
        log_tag=LogTags.test_2,
        is_static_or_classmethod=True
    )
    def static_method(value=2):
        logger.log(
            msg=f'The logger wrapper for this method is overridden.',
            log_tag=LogTags.test_1
        )
        return {
            'value': value
        }

    @staticmethod
    def _pause():
        logger.log(
            msg=f'The logger should wrap this method with wrap_class().',
            log_tag=LogTags.test_1
        )
        time.sleep(0.1)


@logger.wrap_func(log_tag=LogTags.test_2)
def thread_test(lex: LoggerExample):
    threads = [threading.Thread(target=lex._pause) for i in range(30)]
    [thread.start() for thread in threads]
    [thread.join() for thread in threads]


@logger.wrap_func()
def test_rules():
    logger.set_rule(
        blacklist_function=lambda x: x.value <= 10 or x.value >= 20,
        why='No logs should appear on the console. '
            'Only log tags with values between 10 and 20 should persist.'
    )
    logger.log('This message should not be logged.')
    logger.log('This message should be logged.', log_tag=LogTags.test_1)
    logger.log('This message should be logged.', log_tag=LogTags.test_2)
    logger.log('This message should not be logged.', log_tag=LogTags.critical)
    logger.set_rule(
        reset=True,
        why='All logs should persist and appear on the console.'
    )


@logger.wrap_func()
def test_property_functions(le: LoggerExample):
    logger.log('Property functions cannot be wrapped by the logger. '
               'Logs must exist within these functions.')
    le.class_var = 3
    logger.log(f'Got {le.class_var}')


@logger.wrap_func()
def test_threading(lex: LoggerExample):
    logger.log('Testing threads.')
    thread_test(lex)



if __name__ == '__main__':
    start = time.time()
    from_dt = datetime.now() - timedelta(seconds=2)
    to_dt = datetime.now() - timedelta(seconds=1)
    with logger.generate(format_generator='html'):
        test_rules()
        le = LoggerExample(5)
        test_property_functions(le)
        le.override_log_wrapper(10)
        le.recursive_function(5)
        le.class_method()
        le.static_method()
        test_threading(le)
        end1 = time.time()
        print(f'Program finished in {end1-start} seconds.')
    end2 = time.time()
    print(f'HTML generation finished in {end2 - end1} seconds.')
