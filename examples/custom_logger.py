from dblogging.config import LogTagTemplate, LogTag


class LogTags(LogTagTemplate):
    default = LogTag(
        name='Default',
        value=0,
        html_color='cyan'
    )

    test_1 = LogTag(
        name='Test 1',
        value=10,
        html_color='orange'
    )

    test_2 = LogTag(
        name='Test 2',
        value=20,
        html_color='green'
    )

    critical = LogTag(
        name='Invalid Guess',
        value=90,
        html_color='red'
    )
